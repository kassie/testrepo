package by.ls.gr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

//    @Override
//    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        ModelAndView model = new ModelAndView("index");
//        model.addObject("msg", "hello world!");
//
//        return model;
//    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "client:index";
    }
}
